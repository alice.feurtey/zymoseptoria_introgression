#/usr/bin/python


#    ***************************************************
# |   This script was written by Alice Feurtey in 2017    |
# |   It was last modified on the 26/04/18                |
#    ***************************************************
import os
import argparse
import sys
try:
    from ete3 import Tree
except:
    print ("")
    print ("ERROR")
    print ("Prerequisite. Execute following command line in the shell before exectuting this script.")
    print ("export PATH=~/anaconda_ete/bin:$PATH")
    print ("")
    print ("If the above is not working, it is possible that you need to install ete3.")

    sys.exit("")


#   <<>><<>><<>><<>><<>><<>>
# |   Functions & classes    |
#   <<>><<>><<>><<>><<>><<>>


def filtering_blocks_on_NA(maf_input_name, min_block_length, min_prop_data, min_samples):
    with open(maf_input_name, "r") as infile_file:
        maf = infile_file.read().strip().split("a score")
    outfile_name = maf_input_name.rsplit(
        ".", 1)[0] + ".filtered.maf"
    count_kept_block = 0
    count_all_block = 0
    count_removed_seq = 0
    count_all_seq = 0
    with open(outfile_name, "w") as out:
        for block in maf:
            if block.startswith("#"):
                out.write(block)
            else:
                count_all_block += 1
                lines = filter(None, block.strip().split("\n"))
                new_lines = []
                keep = 0
                # Let's check the length of the alignment
                aln_length = len(filter(None, lines[-1].strip().split())[-1])
                if aln_length >= min_block_length:
                    for line in lines:
                        parts = filter(None, line.strip().split())
                        if line.startswith("="):
                            new_lines.append(line)
                        else:
                            count_all_seq += 1
                            # Let's keep only the sequences with more that a set proportion of data compared to the alignment.
                            if float(parts[3]) >= float(aln_length * min_prop_data):
                                new_lines.append(line)
                                keep += 1
                            else:
                                count_removed_seq += 1
                    if keep >= min_samples:
                        count_kept_block += 1
                        out.write("a score" + "\n".join(new_lines) + "\n\n")
    print "This filter removed", count_removed_seq, "sequences and there were", count_all_seq, "sequences in total."
    print "The number of kept blocks is", count_kept_block, "and there were", count_all_block, "blocks in total."
    return outfile_name


# def AF_write_record_with_updates(record):
#     """Write a single SeqRecord object to an 's' line in a MAF block. This has been shamelessly stolen from Biopython and all the work is theirs. """
#     # convert biopython-style 1/-1 strand to MAF-style +/- strand
#     if record.annotations.get("strand") == 1:
#         strand = "+"
#     elif record.annotations.get("strand") == -1:
#         strand = "-"
#     else:
#         strand = "+"
#
#     fields = ["s",
#               # In the MAF file format, spaces are not allowed in the id
#               "%-40s" % record.id.replace(" ", "_"),
#               "%15s" % record.annotations.get("start", 0),
#               "%5s" % record.annotations.get(
#                   "size", len(str(record.seq).replace("-", ""))),
#               strand,
#               "%15s" % record.annotations.get("srcSize", 0),
#               str(record.seq)]
#     return "%s\n" % " ".join(fields)
#
#
# def write_maf_block(open_file, a_score, list_of_records_as_strings):
#     open_file.write("".join(["a score=", str(a_score), "\n"]))
#     open_file.write("".join(list_of_records_as_strings))
#     open_file.write("\n")


# from Bio import AlignIO
# from Bio import SeqIO
# def filtering_blocks_on_NA(maf_input_name, min_block_length, min_prop_data, min_samples):
#     outfile_name = maf_input_name.rsplit(
#         ".", 1)[0] + ".filtered.maf"
#     out = open(outfile_name, "w")
#     count_kept_block = 0
#     count_all_block = 0
#     count_removed_seq = 0
#     count_all_seq = 0
#
#     # Writing the maf headers in the filtered maf output
#     with open(maf_input_name, "r") as maf_open_file:
#         indice = 0
#         while indice < 100:
#             indice += 1
#             temp = maf_open_file.readline
#             if temp.startswith("#"):
#                 out.write(temp)
#         out.write("\n")
#
#     # Now reading the actual alignments
#     for aln in AlignIO.parse(open(maf_input_name, "r"), "maf"):
#         count_all_block += 1
#         enough_data = True
#         kept_records = []
#         aln_length = float(aln.get_alignment_length())
#         if aln_length >= min_block_length:
#             for record in aln:
#                 count_all_seq += 1
#                 #sample_name = record.name.split(".")[0]
#                 if float(record.annotations["size"]) >= float(aln_length * prop_threshold):
#                     kept_records.append(record)
#                 else:
#                     count_removed_seq += 1
#             if len(kept_records) >= min_samples:
#                 count_kept_block += 1
#                 records_to_write = [AF_write_record_with_updates(
#                     rec) for rec in kept_records]
#                 write_maf_block(out, aln._annotations.get(
#                     "score", "0.0"), records_to_write)
#
#     print "This filter removed", count_removed_seq, "sequences and there were", count_all_seq, "sequences in total."
#     print "The number of kept blocks is", count_kept_block, "and there were", count_all_block, "blocks in total."
#     return outfile_name


#   <<>><<>><<>><<>><<>><<>><<>>
# |  Inputs, info and warnings  |
#   <<>><<>><<>><<>><<>><<>><<>>


parser = argparse.ArgumentParser(
    description='This script takes a file with several trees in newick format and the corresponding maf file and allows to count how many of them group samples in a particular way.')
parser.add_argument("-m", "--maf_inputfile", help="This is a maf input. If the option compute_trees is NOT used, then it is assumed that a corresponding file exists containing the trees for each block and named the same way but ending with dnd instead of maf. If the option IS used, then new files will be created with additionnal splitting and filtering of the blocks.", required=True)
parser.add_argument("--compute_trees", action='store_true')
parser.add_argument("--filtering", action='store_true', help="This filtering option will use a minimum block length and a minimum proportion of data to get rid of sequences that do not meet this requirement. It will also remove blocks that are too short (min_block_length) or that don't contain enough sequences: other_samples + 1. (Note that this could be changed to a more complex filter with the name of the samples, but I was too lazy to do it.)")
parser.add_argument("--samples_to_group", nargs='+', required=True)
parser.add_argument("--other_samples", nargs='+', required=True)
parser.add_argument("--ref", required=True)
parser.add_argument("--outgroup", required=True)
parser.add_argument("--min_block_length", default=500.0, type=float)
parser.add_argument("--min_prop_data", default=0.8, type=float)
parser.add_argument("--verbose", action='store_true')
arguments = vars(parser.parse_args())

input_name = arguments["maf_inputfile"]
compute_trees = arguments["compute_trees"]
filtering = arguments["filtering"]
all_Zt_isolates = arguments["samples_to_group"]
wild_species = arguments["other_samples"]
outgroup = arguments["outgroup"]
ref = arguments["ref"]
min_block_length = arguments["min_block_length"]
min_prop_data = arguments["min_prop_data"]
species = {x: "of_interest" for x in all_Zt_isolates}
species.update({x: "other" for x in wild_species})
input_prefix = input_name.rsplit(".", 1)[0]
create_simple_input = "YES"
verbose = arguments["verbose"]

print ""
print ""
print "Your input file name is", input_name
# compute_trees=True


#   <<>><<>><<>>
# |  Script body |
#   <<>><<>><<>>

if compute_trees:
    if filtering:
        print ""
        print ""
        print "Now filtering each block on missing data. Sequences with less than the alignment length multiplied by ", min_prop_data, "pb of data per block will not be kept in the final alignment. Blocks are only kept when they contain more than", 1 + len(wild_species), "sequences."
        maf_input_temp = filtering_blocks_on_NA(
            input_prefix + ".maf", min_block_length, min_prop_data, 1 + len(wild_species))
        maf_input = maf_input_temp.rsplit(".", 1)[0] + ".trees.maf"
        whole_trees_input = maf_input.rsplit(".", 1)[0] + ".dnd"
        print ""
        print ""
    else:
        maf_input_temp = input_name
        maf_input = maf_input_temp.rsplit(".", 1)[0] + ".trees.maf"
        whole_trees_input = maf_input.rsplit(".", 1)[0] + ".dnd"

    with open("/home/feurtey/Documents/First/Scripts/paramfile_temp", "w") as temp:
        temp.write("input.file=" + maf_input_temp + "\n")
        temp.write("input.file.compression=none\n")
        temp.write("input.format=Maf\n")
        temp.write("output.log=temp.log\n")
        temp.write("maf.filter= \\\n")
        temp.write(
            " Subset(species=(Zpass_SP63), strict = yes, keep = yes),  \\\n")
        temp.write(" Output(file=" + maf_input + ", compression=none)")
    os.system(
        "/home/feurtey/.local/bin/bin/maffilter param=/home/feurtey/Documents/First/Scripts/paramfile_temp")

    print ""
    print ""

    with open("/home/feurtey/Documents/First/Scripts/paramfile_temp", "w") as temp:
        temp.write("input.file=" + maf_input + " \n")
        temp.write("input.file.compression=none \n")
        temp.write("input.format=Maf \n")
        temp.write("output.log=temp.log \n")
        temp.write("maf.filter= \\\n")
        temp.write("  DistanceEstimation(\\\n")
        temp.write(
            "   method=ml, gap_option=no_gap, unresolved_as_gap=no, model=K80(kappa=2), \\\n")
        temp.write(
            "   rate=Constant, profiler=none, message_handler=none, parameter_estimation=initial, gaps_as_unresolved=yes), \\\n")
        temp.write("  DistanceBasedPhylogeny(\\\n")
        temp.write("   method=bionj, dist_mat=MLDistance), \\\n")
        temp.write(
            "  NewOutgroup(tree_input=BioNJ, tree_output=BioNJ, outgroup=Zpass_SP63), \\\n")
        temp.write("  OutputTrees(tree=BioNJ, compression=none, \\\n")
        temp.write("   file=" + whole_trees_input + ")")
    os.system(
        "/home/feurtey/.local/bin/bin/maffilter param=/home/feurtey/Documents/First/Scripts/paramfile_temp")

    print ""
    print ""

else:
    maf_input = input_name
    whole_trees_input = input_name.rsplit(".", 1)[0] + ".dnd"


# For easier human reading, the outputs of maffilter are simplified to erase any information that is not the name of the samples and their grouping.

if create_simple_input == "YES":
    trees_input = whole_trees_input + ".simplified"
    os.system("cp " + whole_trees_input + " " + trees_input)
    os.system("sed -i -E 's/\.unitig_([0-9]+)\|quiver\:/\:/g' " + trees_input)
    os.system("sed -i -E 's/\.contig([0-9]+)\:/\:/g' " + trees_input)
    os.system("sed -i -E 's/\.unitig_([0-9]+)\:/\:/g' " + trees_input)
    os.system("sed -i -E 's/\.scaf([0-9]+)\:/\:/g' " + trees_input)
    os.system("sed -i -E 's/\.chr([0-9]+)\:/\:/g' " + trees_input)
    os.system("sed -i -E 's/_chr([0-9]+)//g' " + trees_input)
    os.system("sed -i -E 's/\.//g' " + trees_input)
    os.system("sed -i -E 's/e-//g' " + trees_input)
    os.system("sed -i 's/-//g' " + trees_input)
    os.system("sed -i -E 's/\:([0-9]+)//g' " + trees_input)
else:
    trees_input = whole_trees_input

print ""
# This is the file containing the simplified Newick format trees.
with open(trees_input, "r") as infile1_file:
    arbres = infile1_file.readlines()
    print "The number of trees in the input file is", len(arbres)

# This is the maf file from which the trees have been made.
with open(maf_input, "r") as infile2_file:
    maf = infile2_file.read()
    # Changed from previous script. If length inequal, go back to previous.
    blocks = [x for x in filter(None, maf.split("a score")) if "#" not in x]


# This is to check that the two files above have the same number of trees.
if len(arbres) == len(blocks):
    print "Same number of trees and blocks:", len(arbres)
else:
    print "Error! Number of trees is", len(arbres), "and number of blocks is", len(blocks)
    indice = min(len(arbres), len(blocks))
    print blocks[indice - 1], blocks[indice], blocks[indice + 1], arbres[indice]
    import sys
    sys.exit()

# Let's create an output
output_per_tree = open(maf_input + ".per_tree.txt", "w")  # NEW
output_per_tree.write("\t".join(["Ref_contig", "Ref_start", "Ref_stop",
                                 "Enough_wild", "Monophyly", "Closest_wild_sp_per_node"]) + "\n")

# In order to find the alignment block more easily, I name them accordingly to the reference.
names = []
count_no_ref = 0
for block in blocks:
    lignes = block.split("\n")
    temp_line = filter(None, str([x for x in lignes if ref in x]).split())
    try:
        names.append(
            [temp_line[1].split(".")[1], temp_line[2], str(int(temp_line[2]) + int(temp_line[3]))])  # Changed to add the stop
    except:
        count_no_ref += 1
        names.append(["No_ref" + str(count_no_ref), "NA", "NA"])

# To create a nested dictionary (first keys = sample names, second keys = wild sample name, values = counter)
dico = {}
for sample in all_Zt_isolates:
    dico[sample] = {}
    for espece in wild_species + ["is_in_tree"]:
        dico[sample][espece] = 0

# To create the variables needed further on
d = 0
wild_samples_in_Zt_groups = []
tree_count_with_wild = 0
tree_count_without_wild = 0
tree_count_without_enough_wild = 0

import sys
if len(names) != len(arbres):
    print "Nb of names:", len(names)
    print "Nb of trees", len(arbres)
    sys.exit("Ask Alice. She probably did something stupid again.")

# Let's start analysing the trees
for arbre in zip(names, arbres):
    to_write = list(arbre[0])
    d += 1
    # newick subformat 1 to read internal node names
    tree = Tree(arbre[1].strip(), format=1)
    leaf_names = tree.get_leaf_names()
    for leaf in tree:
        leaf.add_features(espece=species.get(leaf.name, "none"))

    if len(list(set(leaf_names).intersection(wild_species))) < len(wild_species):
        tree_count_without_enough_wild += 1
        to_write.extend(["No", "NA", "NA"])
    else:  # if all the wild species are present in the tree
        to_write.append("Yes")
        count_Zt_in_tree = 0
        for leaf in leaf_names:
            if leaf in all_Zt_isolates:
                dico[leaf]["is_in_tree"] += 1
                count_Zt_in_tree += 1

        if count_Zt_in_tree > 0:
            is_monophyly = tree.check_monophyly(
                values=["of_interest"], target_attr="espece", ignore_missing=True)
            if is_monophyly[1] == "monophyletic":
                tree_count_without_wild += 1
                to_write.extend(["Yes", "NA"])
            else:
                tree_count_with_wild += 1
                to_write.append("No")
                closest = []
                print ""
                print arbre
                print tree.get_ascii(attributes=[
                    "name", "espece"], show_internal=False)

                for node in tree.get_monophyletic(values=["of_interest"], target_attr="espece"):
                    print node.get_ascii(attributes=[
                        "name", "espece"], show_internal=False)
                    parent_node = node.get_ancestors()[0]
                    Zt_in_subtree = node.get_leaf_names()
                    # print (Zt_in_subtree)
                    # This list is to find all the isolates under the node before each monophyletic subtree.
                    # List all isolates and keep only non Zt to identify the wild isolates.
                    temp = []
                    for isolate in list(set(leaf_names) - set(parent_node.get_leaf_names())):
                        if species[isolate] != "of_interest":
                            temp.append(isolate)
                    if temp == [outgroup]:
                        print "The cluster is outside."
                        for Zt in Zt_in_subtree:
                            dico[Zt][outgroup] += 1
                        closest.append("Zpass")
                    else:
                        closest_wild = list(set(wild_species).intersection(
                            set(parent_node.get_leaf_names())))
                        print "The other species closest to this node:", closest_wild
                        for Zt in Zt_in_subtree:
                            for wild in closest_wild:
                                dico[Zt][wild] += 1
                        closest.append(",".join(closest_wild))
                to_write.append(";".join(closest))
        else:
            to_write.extend(["NA", "NA"])
    output_per_tree.write("\t".join(to_write) + "\n")
    # print "\t".join(to_write) + "\n"

print ""
print "Over the", len(zip(names, arbres)), "trees:"
print " -", count_no_ref, "had lost the reference sequence in filtering"
print " -", tree_count_without_enough_wild, "had less than 4 other species present"
print " -", tree_count_without_wild,  "grouped all samples of interest together in the tree, without any other isolates."
print " -", tree_count_with_wild, "placed some samples of interest closer to at least one other sample than to other samples of interest. These other samples were:"
# for wild in wild_species :
# print ("   -", wild, wild_samples_in_Zt_groups.count(wild), "times")
print "\t".join([" "] + all_Zt_isolates)


for valeur in ["is_in_tree"] + wild_species:
    to_write = []
    for Zt in all_Zt_isolates:
        to_write.append(str(dico[Zt][valeur]))
    print "\t".join([valeur] + to_write)

output_per_tree.close()
