

#  ---------
# |  Trees  |
#  ---------
export PATH=~/anaconda_ete/bin:$PATH;
python ~/Documents/First/Scripts/Compute_and_analyses_trees.py  \
  -m /home/feurtey/Documents/First/SPAdes/Introgressed_regions/Zt_tba_all_outgroups_projectedonZt09_minlen1000_realigned.filtered_on_intrasp.filtered_on_intersp.windows.maf \
  --samples_to_group Zt02 Zt04 Zt05_pacbio Zt07 Zt09 Zt10_pacbio Zt11 Zt148 Zt150 Zt151 Zt154 Zt153 Zt155 SRR2834988  SRR2835000 SRR2835044 SRR2835085 SRR2866532 SRR2866536 SRR2834987 SRR2834990 SRR2835032 SRR2835057 SRR2835094 SRR2866534 SRR2866537  \
  --other_samples Zp13_pac_assembly Zb87_pac_assembly Za17_pac_assembly Zpass_SP63  \
  --ref Zt09  --outgroup Zpass_SP63  \
  --compute_trees  \
   --filtering --min_block_length 500 --min_prop_data 0.8 \
  > /home/feurtey/Documents/First/SPAdes/Introgressed_regions/Zt_tba_all_outgroups_projectedonZt09_minlen1000_realigned.filtered_on_intrasp.filtered_on_intersp.windows.stats_trees

python ~/Documents/First/Scripts/Compute_and_analyses_trees.py  \
  -m /home/feurtey/Documents/First/SPAdes/Introgressed_regions/Zt_tba_all_outgroups_projectedonZt09_minlen1000_realigned.filtered_on_intrasp.filtered_on_intersp.windows.filtered.trees.maf \
  --samples_to_group Zt02 Zt04 Zt05_pacbio Zt07 Zt09 Zt10_pacbio Zt11 Zt148 Zt150 Zt151 Zt154 Zt153 Zt155 SRR2834988  SRR2835000 SRR2835044 SRR2835085 SRR2866532 SRR2866536 SRR2834987 SRR2834990 SRR2835032 SRR2835057 SRR2835094 SRR2866534 SRR2866537  \
  --other_samples Zp13_pac_assembly Zb87_pac_assembly Za17_pac_assembly Zpass_SP63  \
  --ref Zt09  --outgroup Zpass_SP63  \
   --filtering --min_block_length 500 --min_prop_data 0.8


#  --------
# |  Gmin  |
#  --------

python /home/feurtey/Documents/First/Scripts/Sum_stats_introgression.py \
 -m /home/feurtey/Documents/First/SPAdes/Introgressed_regions/Zt_tba_all_outgroups_projectedonZt09_minlen1000_realigned.filtered_on_intrasp.filtered_on_intersp.windows.maf \
 -r Zt09 \
 --ingroup Zt02 Zt04 Zt05_pacbio Zt07 Zt09 Zt10_pacbio Zt11 Zt148 Zt150 Zt151 Zt154 Zt153 Zt155 SRR2834988 SRR2835000 SRR2835044 SRR2835085 SRR2866532 SRR2866536 SRR2834987 SRR2834990 SRR2835032 SRR2835057 SRR2835094 SRR2866534 SRR2866537 \
 --groups Zp13_pac_assembly \
 --groups Zb87_pac_assembly \
 --groups Za17_pac_assembly \
 --outgroup Zpass_SP63 \
 --min_samples_per_pop 1 --min_block_length 500 \
 --min_prop_data 0.8 \
 -o /home/feurtey/Documents/First/SPAdes/Introgressed_regions/Zt_tba_all_outgroups_projectedonZt09_minlen1000_realigned.filtered_on_intrasp.filtered_on_intersp.windows.distances_stats.txt
