#/usr/bin/python

import argparse
import os
from subprocess import check_call, CalledProcessError
import sys

# Beware: this script was only created for a maf file where the reference species was present in all blocks (maf file projected on the reference).
# This script takes a maf input, lists the contigs per block for the samples specified in the list below in the first output and then make each sample contigs correspond to the reference chromosomes

# READ ME: To understand the different options, it is easier to just type in your shell: python Associates_contigs_to_chromosomes.py --help
# The same information will be displayed but it is easier to understand.


#   <<>><<>><<>><<>><<>><<>>
# |   Functions & classes    |
#   <<>><<>><<>><<>><<>><<>>


def find_sample_line_in_maf_block(sample_name, block, verbose=False):
    """ Thanks a maf bloc and a sample name. returns none if the sample is not found. Otherwise, returns the line. """
    lignes = block.split("\n")
    temp_line = filter(
        None, [ligne for ligne in lignes if ligne.startswith("s") and sample_name == ligne.split()[1].split(".")[0]])
    # print ""
    # print sample_name, temp_line
    if len(temp_line) == 0:
        if verbose:
            print "ERROR:", sample_name, "has not been found in the block starting with", block[:100]
        return None
    else:
        if len(temp_line) > 1:
            # This if is added for the special case where the sample name is included in another sample name (ex: ref=="Sp1", other_sample=="Sp10").
            for i in temp_line:
                if i.split()[1].split(".")[0] == sample_name:
                    return i
                    break
            else:
                if verbose:
                    print "ERROR:", sample_name, "has not been found in the block starting with", block[:100]
                return None
        else:
            return temp_line[0]


def get_coords_fragment(unsplit_line, convert_to_plus=True, with_ind=False):
    """ Given a maf s line, returns the chromosome, strand, start and end converted to the plus strand or not. """
    line_Sp = unsplit_line.split()
    if with_ind:
        ind = line_Sp[1].split(".")[0]
    try:
        chrom = line_Sp[1].split(".")[1]
    except:
        if unsplit_line.startswith("#"):
            pass
        else:
            print "I could not get coordinates from this line:", unsplit_line
    strand = line_Sp[4]
    if convert_to_plus and ref_strand == "-":
        coords = [int(line_Sp[5]) - int(line_Sp[2]), int(line_Sp[5]) -
                  (int(line_Sp[2]) + int(line_Sp[3]))]
        start = min(coords)
        end = max(coords)
    else:
        start = int(line_Sp[2])
        end = int(line_Sp[3]) + start
    if with_ind:
        return [ind, chrom, str(strand), str(start), str(end)]
    else:
        return [chrom, str(strand), str(start), str(end)]


#   <<>><<>><<>><<>><<>><<>><<>>
# |  Inputs, info and warnings  |
#   <<>><<>><<>><<>><<>><<>><<>>

#   -----------
#  |  Inputs   |
#   -----------


parser = argparse.ArgumentParser(description='This script takes an input maf file. It will then create two outputs. The first one is a list indicating for each block and each sample selected the contig corresponding. The second one will be based on this first output and will create a table with for each sample the contigs corresponding to the reference chromosomes (or contigs). Two threshold are taken into account to determine which contigs correspond to which chromosomes. See below to read the description of these two threshold. \nBEWARE: this script was only created for a maf file where the reference species was present in all blocks (maf file projected on the reference).', add_help=True)

# Input/output
parser.add_argument("-m", "--maf_inputfile", type=str, required=True,
                    help="This is the alignment input file name in maf format.BEWARE: This script has not be tested on a non-projected maf. It should be able to take them into account, but check you results and contact me if anything weird happens.")
parser.add_argument("-o", "--outputs_name", type=str, help=str(
    "This is an optional output name. If this option is not specified, the output files names will be based on the input file name and the threshold."))
parser.add_argument("-l", "--listinputfile", help="OPTIONAL. Add only if you are running the third step only. This is a table  with the contigs corresponding to each chromosome (Format: tab file ; col = sample ; line = chromosome ; first col = \"#Chromosome\" ; next cols = list of contigs separated by \",\" without spaces.) It usually is the output from the first two steps of this script and if you run all steps ", type=str)

# Samples
parser.add_argument("-r", "--ref", type=str, required=True,
                    help=str("This is the ID of the reference."))
parser.add_argument("-s", "--samples", nargs='+',
                    help=str("This is a list of samples."), required=True)

# Filter thresholds
parser.add_argument("--threshold_prop", type=float, default=0.9, help=str(
    "This is a float that gives a proportion threshold for a contig to be attributed to one chromosome. The default is 0.9 which means that to be attributed to one chromosome, the contig must have more than 90 percent of its aligned length (not its total length) on this particular chromosome."))
parser.add_argument("--threshold_length", type=int, default=1000000000, help=str(
    "This is an integer or number that gives a length threshold. Above this alignment length, the contig will be associated with the chromosome regardless of any proportion. The default is set on one billion so as to be higher than can be reached."))
parser.add_argument("--min_aligned_length", type=int, default=0, help=str(
    "This is an integer or number that gives a minimum total aligned length threshold. This is useful in order to remove any contig that would only align on a very small part of the alignement. The default, however, is 0 so that no contig will be filtered out based on this condition."))

# Filter options
parser.add_argument("-k", "--keep_all", action='store_const', const="YES", help=str(
    "If this flag is added, then all the samples present in the maf input will be present in the maf output. If not (default), only the samples present in the list input will be present in the maf output."))
parser.add_argument("--unprojected", action='store_true', help=str("The default is to work on an alignment that has been projected onto the given reference. If so, the blocks are all expected to contain the reference sequence. If that it not the case, the block is treated as an error and filtered out on step 3. If your alignment is not projected on the sample that you indicated as the reference, then use this option to also keep blocks that do not contain this sample. These will, of course, not be filtered in any way by this script."))

# Other options
parser.add_argument("-w", "--which_step", default="All", help=str("This value is to indicate which steps of the script will be used. The default executes the whole script and we recommend to not touch it for simpler use. If you really want to, you can also use the values \"First\",  \"Second\" and \"Third\". The first part of the script will create a list of blocks with the contigs for the samples of interest. The second part is the one that associates contigs to chromosome. The third step will filter your input maf file based on the results from the first two steps."))
parser.add_argument("--convert_to_plus", action="store_true", help=(
    "If this option is used, all the coordinates will be converted to the plus strand. The default is to keep the coordinates as in the maf file."))
args = parser.parse_args()
arguments = vars(args)

input_name = arguments["maf_inputfile"]
ref = arguments["ref"]
threshold_value_proportion = arguments["threshold_prop"]
threshold_value_length = arguments["threshold_length"]
samples = arguments["samples"]
output_name = arguments["outputs_name"]
parts = arguments["which_step"]
min_aligned_length = arguments["min_aligned_length"]


# Creation of the output names either based on the input name or based on the user specified string.
if output_name == None:
    input_prefix = input_name.rsplit(".", 1)[0]
    output1_name = input_prefix + "_block_list_with_contigs.txt"
    output2_name = input_prefix + "_contigs_per_chromosomes_thresholds_" + str(
        threshold_value_proportion) + "_" + str(threshold_value_length) + ".txt"
    filtered_maf_file = input_prefix + ".filtered.maf"
else:
    output1_name = output_name + "_block_list_with_contigs.txt"
    output2_name = output_name + "_contigs_per_chromosomes.txt"
    filtered_maf_file = output_name + ".maf"

#   ----------------------
#  |  Infos and warnings  |
#   ----------------------

if parts == "All":
    print ""
    print ""
    print "Your maf input file name is", input_name
    print "Your output files names are:"
    print "  - list of contigs per block per sample:", output1_name
    print "  - lists of contigs to keep per reference chromosome per sample:", output2_name
    print "  - filtered maf file:", filtered_maf_file
    print ""
    print "Your filters are :"
    print "  - proportion threshold:", threshold_value_proportion
    print "  - length threshold", threshold_value_length
    print "  - minimum total aligned length threshold", min_aligned_length
    print ""
    print "Your reference name is", ref, "and your samples are", ", ".join(samples)
    print ""
    first_step = True
    second_step = True
    third_step = True

elif parts == "First":
    print ""
    print ""
    print ""
    print "BEWARE!! You are executing only the first step of the script. The file", output2_name, "will not be created. This means that you will only get a list of blocks and the corresponding chromosomes/contigs for the chosen samples."
    first_step = True
    second_step = False
    third_step = False

elif parts == "Second":
    print ""
    print ""
    print ""
    print "BEWARE!! You are executing only the second step of the script. The file", output1_name, "has to exist. You may have changed it manually. In general, we do not recommend this option but it does exist for speed issue should you have to change your filtering threshold."
    first_step = False
    second_step = True
    third_step = False

elif parts == "Third":
    print ""
    print ""
    print ""
    print "BEWARE!! You are executing only the third step of the script. The files", output1_name, "and", output2_name, "have to exist."
    print "The output file name is:", filtered_maf_file
    first_step = False
    second_step = False
    third_step = True

else:
    raise SystemExit(
        "The accepted values for the option 'which_step' are First, Second, Third or All.")


#   <<>><<>><<>>
# |  Script body |
#   <<>><<>><<>>
print "\n\n"
print "#   --------------"
print "#  |  First part  |"
print "#   --------------"
# The first part of the script will create an output that lists the blocks and the corresponding chromosomes/contigs.

if first_step:
    # Here is the script
    with open(input_name, "r") as infile:
        maf = infile.read()
        blocks = filter(None, maf.split("\na score"))

    out1 = open(output1_name, "w")
    out1.write("#Chromosome\tRef_strand\tRef_start\tRef_end\t" + ref + "\t" +
               "\t".join(samples) + "\n")

    c = 0
    for block in blocks:
        # Let's first reset some values...
        ref_line = None
        ref_chrom = ""

        #...and go
        if block.startswith("#"):  # if the block is a comment block.
            pass
        else:
            c += 1
            if c % 10000 == 0:
                print "Looking at block number", c

            # If the file is supposed to be projected on the reference sample given, I will give an error for each block found without the reference sample and then ignore the block. Otherwise, the block is ignored without any warning.n
            if args.unprojected:
                ref_line = find_sample_line_in_maf_block(ref, block, False)
            else:
                ref_line = find_sample_line_in_maf_block(ref, block, True)

            if ref_line:  # That is to say, if the block contains the reference
                # We find the start and end position of the block (to add them to the file).
                ref_coords = get_coords_fragment(
                    ref_line, args.convert_to_plus)
                contigs = [(";".join(ref_coords))]

                # Then we loop over the sample to find their line in this block.
                for sample in samples:
                    temp_line = find_sample_line_in_maf_block(sample, block)
                    if temp_line:
                        contigs.append(
                            ";".join(get_coords_fragment(temp_line, args.convert_to_plus)))
                    else:
                        contigs.append(";".join(["NA", "NA", "NA", "NA"]))

                # After the contigs have been identified for all the samples from the list, the information for the block is written in the output file.
                out1.write("\t".join(ref_coords + contigs) + "\n")

out1.close()
print "The list of contigs per block has been created and is written in the first output", output1_name
print ""


print "\n\n"
print "#   ---------------"
print "#  |  Second part  |"
print "#   ---------------"

# The second part of the script is now using the output from the first part as an input.

if second_step:
    # Reading the output from the first part as input for this new part. Returns header (list), table (list of lists) and chromosomes (list).
    with open(output1_name, "r") as infile:
        header = infile.readline().strip().split()
        infile1 = infile.readlines()
        table = []
        chromosomes = []
        for i in infile1:
            temp = i.strip().split()
            table.append(temp)
            chromosomes.append(temp[0])
        chromosomes = sorted(set(chromosomes))

    print "Your chromosomes are", ", ".join(chromosomes), "\n"

    # Setting up the final file
    header_to_write = ["#Chromosome"]
    contigs_per_sample_per_chromosome = {}
    for chromosome in chromosomes:
        contigs_per_sample_per_chromosome[chromosome] = [chromosome]

    for c, sample in enumerate(header[4:]):
        header_to_write.append(sample)
        # For each sample, a dictionnary will be created with the reference contigs/chromosomes as keys and the names of the corresponding contigs for the sample as value (format: list).
        contigs_per_chr = {}
        for chromosome in chromosomes:
            contigs_per_chr[chromosome] = []
        print "\nWe are now looking at sample", sample
        contigs1 = []  # List of all the contigs for the sample
        # List of tuples containing (contig name, corresponding chromosome) for all the blocks for the sample
        contigs2 = []
        for i in table:
            sample_info = i[c + 4].split(";")
            if sample_info[0] != "NA":
                contigs1.append(sample_info[0])
                contigs2.append(
                    (sample_info[0], i[0], (int(i[3]) - int(i[2]))))
        contigs1 = set(contigs1)
        for contig in contigs1:  # For all the contigs for this sample, creating a dictionnary with the corresponding chromosomes as key and the length of alignment between the contig and the chromosome as value.
            dico = {}
            for info in [x for x in contigs2 if x[0] == contig]:
                chromo = info[1]
                if chromo in dico:
                    dico[chromo] += int(info[2])
                else:
                    dico[chromo] = int(info[2])
            # For each contig, I am now setting up the threshold value in bp based on the aligned length and the proportion threshold set as input.
            seuil = float(sum(dico.values())) * \
                float(threshold_value_proportion)

            # Now looking at whether any threshold (either proportion of aligned length or total length) is met for any chromosome
            is_prop_found = "NO"
            is_length_found = "NO"
            for chromo in dico:
                if float(dico[chromo]) >= seuil:
                    if float(dico[chromo]) >= min_aligned_length:
                        contigs_per_chr[chromo].append(contig)
                        # This contig is associated to one chromosome with a proportion superior or equal to the threshold (threshold_value_proportion) set in the beginning.
                        is_prop_found = "YES"
                elif float(dico[chromo]) >= threshold_value_length:
                    contigs_per_chr[chromo].append(contig)
                    # This contig is associated to one chromosome with a length superior or equal to the threshold (threshold_value_length) set in the beginning.
                    is_length_found = "YES"

            # No unique chromosome could be attributed to this contig. The info will thus be printed on the screen.
            if is_prop_found == "NO":
                if is_length_found == "NO":
                    print "     No association found for", contig, dico
                elif is_length_found == "YES":
                    print "     No unique chromosome was found for", contig, dico, "but", contig, "is associated to some chromosome with a length superior to the threshold."
            elif is_prop_found == "YES":
                if is_length_found == "NO":
                    pass
                elif is_length_found == "YES":
                    print "     One chromosome was associated to", contig, dico, " with a higher proportion than the threshold but", contig, "is also associated to another chromosome with a length superior to the threshold."

        # I record all the associated contigs in the dictionary "contigs_per_sample_per_chromosome".
        for chromosome in chromosomes:
            if len(contigs_per_chr[chromosome]) > 0:
                contigs_per_sample_per_chromosome[chromosome].append(
                    ",".join(contigs_per_chr[chromosome]))
            else:
                contigs_per_sample_per_chromosome[chromosome].append("NA")

    # Based on the record in the dictionary "contigs_per_sample_per_chromosome", I can now write my output.
    with open(output2_name, "w") as out2:
        out2.write("\t".join(header_to_write))
        for chromosome in chromosomes:
            out2.write(
                "\n" + "\t".join(contigs_per_sample_per_chromosome[chromosome]))

    print ""
    print ""
    print "For each sample, a list has been printed with the contigs that could not be attributed to any unique chromosome based on your threshold value of", threshold_value_proportion, ". Should you not like this list, you can either: change the threshold or manually edit the last output file", output2_name + "."
    print ""
    print ""


print "\n\n"
print "#   --------------"
print "#  |  Third step  |"
print "#   --------------"
if third_step:

        #    Reading inputs
        #   ----------------
    if not first_step:
        with open(maf_input_name, "r") as infile:
            maf = infile.read()
            blocks = filter(None, maf.split("\na score"))

    with open(output2_name, "r") as in_liste:
        samples = in_liste.readline().strip().split()[1:]
        liste = in_liste.readlines()
        # This is a dictionnary of dictionnaries. The keys of the main dictionnary are the chromosomes. The keys of the sub-dictionnaries are the name of the samples and the values of the contigs to be kept.
        to_keep = {}
        for k in liste:
            temp = k.strip().split("\t")
            to_keep[temp[0]] = {}
            count = 1
            for sample in samples:
                to_keep[temp[0]][sample] = temp[count].strip().split(",")
                count += 1

    out = open(filtered_maf_file, "w")

    #     Comparing and processing
    #   ---------------------------

    # This is a dictionary which will contain the information of which sequences (values) are filtered out from each blocks (keys, based on reference).
    not_kept = {}
    seq_nb = 0
    seq_nb_not_kept = 0
    d = 0
    other_samples = 0
    for block in blocks:
        # Let's first reset some values...
        filtered_block = []
        temp_line_S = []

        #...and go
        if block.startswith("#"):  # if the block is a comment block.
            out.write(block)
        else:
            d += 1
            if d % 10000 == 0:
                print "We are looking at block number", d
            if args.unprojected:
                ref_line = find_sample_line_in_maf_block(ref, block, False)
            else:
                ref_line = find_sample_line_in_maf_block(ref, block, True)

            if ref_line:  # That is to say, if the block contains the reference
                lignes = filter(None, block.strip().split("\n"))
                seq_nb += len(lignes) - 1
                ref_chrom, ref_strand, ref_start, ref_end = get_coords_fragment(
                    ref_line, args.convert_to_plus)

                for ligne in lignes:
                    if ligne.startswith("="):
                        # If the line is the first of the block (the header), we keep it to be in the output.
                        filtered_block.append(ligne)
                    else:  # If the line is actually a sequence line...
                        temp_ind = None
                        try:
                            temp_ind, temp_chrom, temp_strand, temp_start, temp_end = get_coords_fragment(
                                ligne, args.convert_to_plus, True)
                        except:
                            print ligne

                        # ... if the current sample is the reference, we keep it to be in the output.
                        if temp_ind == ref:
                            filtered_block.append(ligne)
                        elif temp_ind in samples:
                            # ... if the reference is also in the sample list, we have kept in already so we do not write it again.
                            if temp_ind == ref:
                                pass
                            # ... if the current sample is in the list and if the contig is one of the kept contig, we keep this line to be in the output.
                            elif temp_chrom in to_keep[ref_chrom][temp_ind]:
                                filtered_block.append(ligne)
                            else:  # ... if the current sample is in the list but the contig is not one of the kept contig corresponding to the reference chromosome, I do not keep this line for the output. Instead, I store it in the unkept dictionary.
                                seq_nb_not_kept += 1
                                if (ref_chrom, ref_start) in not_kept:
                                    not_kept[(ref_chrom, ref_start)
                                             ].append(temp_ind)
                                else:
                                    not_kept[(ref_chrom, ref_start)] = [
                                        temp_ind]
                        else:  # ...and if the current sample is not in the list file...
                            if args.keep_all == "YES":  # ... we keep it only if the option keep_all is selected.
                                filtered_block.append(ligne)
                            else:
                                other_samples += 1

                out.write("\n\na score" + "\n".join(filtered_block))
            else:
                if args.unprojected:
                    out.write("\n\na score" + block)
    out.close()

    #    Final info display
    #   ---------------------
    # print not_kept
    if args.keep_all == "YES":
        print "Overall, we inspected", d, "blocks including", seq_nb, "sequences. From these, we discarded", seq_nb_not_kept, "sequences coming from", len(not_kept), "different blocks."
    else:
        print "Overall, we inspected", d, "blocks including", seq_nb, "sequences. From these, we discarded", seq_nb_not_kept, "sequences coming from", len(not_kept), "different blocks."
        if other_samples > 0:
            print "We have discarded", other_samples, "additionnal sequences because they did not belong to the samples you gave."
print "\nYour filtered maf file is", filtered_maf_file
