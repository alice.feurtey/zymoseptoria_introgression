The files found in this repository are the one used for generating the analyses presented in the Feurtey et al. manuscript submission.
Here, I will describe what each of them was used for in the order in which they are used in the pipeline.

* The python script Filter_maf_based_on_synteny.py was used in order to remove sequences which seemed aligned at the wrong locus based on synteny in the different MGAs. 
A visual of the filtering process can be found as a supplementary figure.

* The files Compute_and_analyses_trees.py and Sum_stats_introgression.py were used to create our two measures of introgression. The exact commands can be found in the commands_trees_and_Gmin.sh.

* The script Zt_all.rmd (and/or pdf knitted version of the script) gets all the statistics and figures used in the paper about the All_Zt_MGA.

* The script Method_comparison_Zt3.rmd (and/or pdf knitted version of the script) does the same for the comparison with the three datasets containing information from the reference genomes and the strains Zt05 and Zt10.


