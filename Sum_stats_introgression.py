#/usr/bin/python


#    *********************************************************
# |  This script was written by Alice Feurtey on the 16/05/18  |
# |  It was last modified on the 26/05/18                      |
#    *********************************************************

from Bio import AlignIO
from Bio.Cluster import distancematrix
from Bio.Phylo.TreeConstruction import DistanceCalculator
import argparse
import egglib
import numpy as np


#   <<>><<>><<>><<>><<>><<>>
# |   Functions & classes    |
#   <<>><<>><<>><<>><<>><<>>

def create_matrix(aln):
    """ Takes an alignment from the Biopython module and returns my version of Gmin. """
    calculator = DistanceCalculator(
        model='identity', skip_letters="-")  # Added skip letters
    dm = calculator.get_distance(aln)
    return(dm)


def find_pairs_in_aln(aln, pop1_samples, pop2_samples):
    current_in_pop1 = []
    current_in_pop2 = []
    for record in aln:
        if record.id in pop1_samples:
            current_in_pop1.append(record.id)
        if record.id in pop2_samples:
            current_in_pop2.append(record.id)
    if any([len(current_in_pop1) == 0, len(current_in_pop2) == 0]):
        return None
    else:
        pairs = [(i, j) for i in current_in_pop1 for j in current_in_pop2]
        return pairs


def make_dist_list(dm, pairs):
    dist_list = []
    if pairs:
        for pair in pairs:
            dist_list.append(dm[pair])
        return dist_list
    else:
        return None


def measure_dmin_AF(dist_list):
    if dist_list:
        return min(dist_list)
    else:
        return "NA"


def measure_dmax_AF(dist_list):
    if dist_list:
        return max(dist_list)
    else:
        return "NA"


def measure_Gmin_AF(dist_list):
    if dist_list:
        return min(dist_list) / np.mean(dist_list)
    else:
        return "NA"


def measure_RNDmin_AF(dist_list_XY, dist_list_Xout, dist_list_Yout):
    """ Takes an alignment from the Biopython module and returns my version of Gmin. """
    if all([dist_list_XY, dist_list_Xout, dist_list_Yout]):
        dout = ((np.mean(dist_list_Yout) + np.mean(dist_list_Xout)) / 2)
        return min(dist_list_XY) / dout
    else:
        return "NA"


#   <<>><<>><<>><<>><<>><<>><<>>
# |  Inputs, info and warnings  |
#   <<>><<>><<>><<>><<>><<>><<>>


parser = argparse.ArgumentParser(
    description='This script takes a multi alignment file and uses egglib to compute summary statistics.', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-m", "--maf_input_file",
                    help="This is the input file name in a maf format. Note that this script could be modified rather easily to take other input format if needed as long as BioPython recognise that format.", required=True)
parser.add_argument("-r", "--reference",
                    help="This is your reference sample name.", required=True)
parser.add_argument("-o", "--output_file", type=str, default="based_on_input",
                    help=str("This is an optional output file name. If none is specified, the output name will be based on the input name ending with .sum_stats.txt"))
parser.add_argument('--ingroup',
                    default=None, nargs="+")
parser.add_argument('--groups',
                    default=None, action='append', nargs="+")
parser.add_argument('--outgroup', default=None, nargs="+")
parser.add_argument('--min_block_length', default=800, help=str(
    'This is a threshold length to decide whether to keep each block.'))
parser.add_argument("--min_prop_data", default=0.8, type=float,
                    help=str("This is a proportion threshold to keep each sequence (not each block)."))
parser.add_argument('--min_samples_per_pop', default=0, type=int, help=str(
    "This is an integer that gives a minimum number of samples per populations. Under this threshold, no statistics will be computed. The default is 0. However, there are no samples in the ingroup nothing is computed anyway."))
parser.add_argument("--statistics", default="default_stats",
                    help=str("This is an optional list of statistics chosen among the statistics that egglib is able to compute. In November 2017, you can find the list at http://mycor.nancy.inra.fr/egglib/py/stats.html#egglib.stats.site_from_align. One supplementary stat is Gmin. If this stat is added, the populations chosen will be the two first populations specified, or the outgroup and the population provided or the outgroup and all the other samples if no population is provided."), nargs='+')
arguments = vars(parser.parse_args())

maf_inputfile = arguments["maf_input_file"]
ingroup = arguments["ingroup"]
print ingroup
populations = arguments["groups"]
threshold = int(arguments["min_block_length"])
temp_output_name = "temp.fasta"
output_file = arguments["output_file"]
reference = arguments["reference"]
outgroup = arguments["outgroup"]
statistics = arguments["statistics"]
print statistics
min_samples_per_pop = arguments["min_samples_per_pop"]
prop_threshold = arguments["min_prop_data"]
NA_ligne = 0

if output_file == "based_on_input":
    output_file = ".".join(
        [maf_inputfile.rsplit(".", 1)[0], "int_stats", "txt"])


to_write = ["Chrom", "Start", "Stop"]

for stat in statistics:
    if statistics == "default_stats":
        measure_Gmin = True
        measure_RNDmin = True
        measure_dmin = True
        measure_dmax = True
    elif stat == "Gmin":
        measure_Gmin = True
    elif stat == "RNDmin":
        measure_RNDmin = True
    elif statistics == "dmin":
        measure_dmin = True
if measure_dmax:
    if outgroup:
        to_write.append("dmax_ingroup_vs_outgp")
    if populations >= 1:
        for indice, pop in enumerate(populations):
            to_write.append("dmax_ingroup_vs_gp" + str(indice))

if measure_dmin:
    if outgroup:
        to_write.append("dmin_ingroup_vs_outgp")
    if populations >= 1:
        for indice, pop in enumerate(populations):
            to_write.append("dmin_ingroup_vs_gp" + str(indice))

if measure_Gmin:
    if outgroup:
        to_write.append("Gmin_ingroup_vs_outgp")
    if populations >= 1:
        for indice, pop in enumerate(populations):
            to_write.append("Gmin_ingroup_vs_gp" + str(indice))

if measure_RNDmin:
    if populations >= 1:
        for indice, pop in enumerate(populations):
            to_write.append("RNDmin_ingroup_vs_gp" + str(indice))

print "Your input is", maf_inputfile


out = open(output_file, "w")
out.write("\t".join(to_write) + "\n")


#   <<>><<>><<>>
# |  Script body |
#   <<>><<>><<>>
for indice, aln in enumerate(AlignIO.parse(open(maf_inputfile), "maf")):
    if indice % 1000 == 0:
        print "Looking at block", indice

    # Let's check if the amount of data in the block is enough.
    enough_data = True
    if int(aln.get_alignment_length()) < threshold:
        enough_data = False
    else:  # if int(aln.get_alignment_length()) >= threshold:
        if len(ingroup) < min_samples_per_pop:
            enough_data = False
        else:
            current_samples = []
            ref_record = [x for x in aln if x.name.split(".")[0] == reference]
            if len(ref_record) > 1:
                print "You have several reference record here. I will use the first one."
            else:
                ref_record = ref_record[0]
            chromosome = ref_record.name.split(".")[1]
            start = ref_record.annotations["start"]
            end = str(
                int(ref_record.annotations["start"]) + int(ref_record.annotations["size"]))
            ref_length = int(ref_record.annotations["size"])
            for indice, record in enumerate(aln):
                sample_name = record.name.split(".")[0]
                if float(record.annotations["size"]) >= float(float(ref_length) * prop_threshold):
                    record.id = record.id.split(".")[0]
                    current_samples.append(sample_name)
                else:
                    print "Bye bye baby", record.name, float(float(ref_length)), float(float(ref_length) * prop_threshold), float(record.annotations["size"])
                    record.id = "Byebyebaby" + str(indice)

            d = 0
            for pop_nb, samples in enumerate([ingroup] + populations + [outgroup]):
                c = 0
                for sample in samples:
                    if sample in current_samples:
                        c += 1
                d += c
                if c < min_samples_per_pop:
                    enough_data = False
            if d < 1:
                enough_data = False

    if enough_data:
        to_write = [chromosome, str(start), str(end)]
        dm = create_matrix(aln)
        Dmaxs = []
        Dmins = []
        Gmins = []
        RNDmins = []
        if outgroup:
            pair_Xout = find_pairs_in_aln(aln, ingroup, outgroup)
            dist_list_Xout = make_dist_list(
                dm, find_pairs_in_aln(aln, ingroup, outgroup))
            if measure_dmax:
                dmax = measure_dmax_AF(dist_list_Xout)
                Dmaxs.append(str(dmax))
            if measure_dmin:
                dmin = measure_dmin_AF(dist_list_Xout)
                Dmins.append(str(dmin))
            if measure_Gmin:
                Gmin = measure_Gmin_AF(dist_list_Xout)
                Gmins.append(str(Gmin))
        if populations:
            for pop in populations:
                pairs_XY = find_pairs_in_aln(aln, ingroup, pop)
                dist_list_XY = make_dist_list(dm, pairs_XY)
                if measure_dmax:
                    dmax = measure_dmax_AF(dist_list_XY)
                    Dmaxs.append(str(dmax))
                if measure_dmin:
                    dmin = measure_dmin_AF(dist_list_XY)
                    Dmins.append(str(dmin))
                if measure_Gmin:
                    Gmin = measure_Gmin_AF(dist_list_XY)
                    Gmins.append(str(Gmin))
                if dist_list_Xout and measure_RNDmin:
                    pair_Yout = find_pairs_in_aln(aln, ingroup, outgroup)
                    dist_list_Yout = make_dist_list(
                        dm, find_pairs_in_aln(aln, ingroup, outgroup))
                    RNDmin = measure_RNDmin_AF(
                        dist_list_XY, dist_list_Xout, dist_list_Yout)
                    RNDmins.append(str(RNDmin))
        if measure_dmax:
            to_write.extend(Dmaxs)
        if measure_dmin:
            to_write.extend(Dmins)
        if measure_Gmin:
            to_write.extend(Gmins)
        if measure_RNDmin:
            to_write.extend(RNDmins)
        out.write("\t".join(to_write) + "\n")
        # print "\t".join(to_write) + "\n"
    else:
        NA_ligne += 1
        if NA_ligne % 1000 == 0:
            print NA_ligne


out.close()
print ""
print "The computations are finished. You can find your results in the file", output_file
print ""
